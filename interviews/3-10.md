# 2021 年 3 月 10 日 面试题

1.执行下面代码会输出什么信息并简单出过程  (百度金融)

```js
function foo() {
  console.log(a);
}
function bar() {
  var a = 3;
  foo();
}
var a = 2;
bar();
```

2.执行下面代码会输出什么信息并简答出过程  (百度金融)

```js
setTimeout(() => {
  console.log(100);
}, 0);
console.log(200);
Promise.reject(function () {
  console.log(300);
});
```
