## 2021 年 3 月 18 日学习计划

##### 知识目标

1. part1 模块一知识点的掌握
2. 函数参数的规范
3. 箭头函数的理解
4. 新特性记忆与掌握

##### 课程进度目标

1. Part 1 · JavaScript 深度剖析
   - 模块 ：模块二 ES 新特性与 TypeScript、JS 性能优化
   - 任务 ：任务一 ECMAScript 新特性
   - 小节：1-20

##### 如何观看课程

- 移动端
  - 应用商店 搜索 拉勾教育
- PC 端
  - 浏览器地址：https://edu.lagou.com/
- 登录：手机号 + 验证码

##### 重要班级链接

- 班级仓库链接：https://gitee.com/lagoufed/fed-e-010

- 预习课程资料：https://gitee.com/lagoufed/fed-e-010/tree/master/prepare

- 每日学习计划汇总：https://shimo.im/mindmaps/ytvXtjVGJ9QwhY8X

- 作业模板（作业怎么写怎么提交）：https://gitee.com/lagoufed/lagoufed-e-task

- 学习平台使用(课程结构，提交作业&笔记规则)：https://gitee.com/lagoufed/fed-e-010/blob/master/prepare/use.md

##### 直播预告

- 内容：1-1 答疑
- 时间：今晚8点钟
- 直播链接：https://kaiwu.lagou.com/big_course/live/view.html?liveId=2692

##### 其他

###### 问题反馈格式 私信 小希 请加我 wx

- 章节名称
- 问题截图
- 问题描述
- 问题代码
