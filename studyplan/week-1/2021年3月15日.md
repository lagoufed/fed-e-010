## 2021 年 3 月 15 日学习计划

##### 知识目标

1. Promise 基本用法的掌握
2. async await 基本用法的掌握

##### 课程进度目标

1. Part 1 · JavaScript 深度剖析
   - 模块 ：模块一 函数式编程与 JS 异步编程、手写 Promise
   - 任务 ：任务二 JavaScript 异步编程
     - 小节：1【概述】- 18【随堂测试】

##### 如何观看课程

- 移动端
  - 应用商店 搜索 拉勾教育
- PC 端
  - 浏览器地址：https://edu.lagou.com/
- 登录：手机号 + 验证码

##### 重要班级链接

- 班级仓库链接：https://gitee.com/lagoufed/fed-e-010

- 预习课程资料：https://gitee.com/lagoufed/fed-e-010/tree/master/prepare

- 每日学习计划汇总：https://shimo.im/mindmaps/ytvXtjVGJ9QwhY8X

##### 其他

###### 问题反馈格式 私信 小希 请加她 wx

- 章节名称
- 问题截图
- 问题描述
- 问题代码
